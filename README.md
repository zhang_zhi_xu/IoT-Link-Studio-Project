# IoT Link Studio Project

#### 介绍
使用VScode进行开发，基于Huawei LiteOS操作系统

#### 软件架构
使用C语言开发


#### 安装教程

1.  安装VScode
2.  在VScode里面安装IoT Link Studio插件

#### 使用说明

1.  Agriculture
    智慧农业综合代码
2.  Agriculture_Transplant_Sensor
    智慧农业传感器移植
3.  Agriculture_Transplant_Sensor_NB
    智慧农业数据上云
4.  Agriculture_Transplant_Sensor_NB_LWM2M
    智慧农业协议移植
5.  Agriculture_Transplant_Sensor_NB_LWM2M_LCD
    智慧农业LCD移植
6.  Agriculture_Transplant_Sensor_NB_LWM2M_LCD_Key
    智慧农业按键设置
7.  Agriculture_Transplant_Sensor_WIFI
    智慧农业WIFI入云

#### 参与贡献

1.  小熊派开源社区
2.  Huawei LiteOS


